FROM ubuntu:latest

LABEL mantainer="Davide Pennica"

ENV DEBIAN_FRONTEND noninteractive

RUN usermod -u 1000 $HOST_UID
RUN groupmod -g 1000 $HOST_GID

RUN apt-get update && apt-get -y upgrade && \
    apt-get -y --fix-missing install apt-utils apache2 \
    php php-cli php-intl php-gd php-json php-mbstring php-xml php-xsl php-zip \
    php-soap php-pear php-mcrypt libapache2-mod-php \
    curl php-curl apt-transport-https vim lynx-cur php-sqlite3 php-mysql \
    supervisor && apt-get clean && rm -r /var/lib/apt/lists/*

ENV APACHE_RUN_USER ${HOST_UID}
ENV APACHE_RUN_GROUP ${HOST_GID}
ENV APACHE_LOG_DIR /var/log/apache2

# inserire gestione delle variabilil d'ambiente con sed

ADD configs/apache2/site.conf /etc/apache2/sites-available/001-site.conf
ADD scripts/start.sh /usr/local/bin/

EXPOSE 80

# enable modules
RUN a2enmod rewrite
RUN phpenmod mcrypt

RUN a2ensite 001-site.conf

#install composer
RUN curl -sS https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer

VOLUME /var/www/

USER www-data

ENTRYPOINT [ "/usr/local/bin/start.sh" ]
